api = 2
core = 7.x

; Libraries
libraries[mercadopago][download][type] = git
libraries[mercadopago][download][url] = https://github.com/mercadopago/sdk-php.git
libraries[mercadopago][download][branch] = master
libraries[mercadopago][download][revision] = f2caa4daa8dd2839e57f3d4cc47db508b5aeb742
libraries[mercadopago][type] = library
